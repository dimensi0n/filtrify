'use strict'

const Persona = use('Persona')

class UserController {
    async signup({ request, auth, response, view }) {
        try {
            const payload = request.only(['email', 'password', 'password_confirmation'])

            const user = await Persona.register(payload)

            await auth.login(user)
            response.redirect('/dashboard')
        } catch(error) {
            return view.render('signup', { error })
        }
    }

    async disconnect({ auth, response }) {
        await auth.logout()
        response.redirect('../')
    }

    async login({ request, auth, response, view }) {
        try {
            const payload = request.only(['uid', 'password'])
            const user = await Persona.verify(payload)
            await auth.login(user)
            response.redirect('/dashboard')
        } catch(error) {
            return view.render('login', { error })
        }
    }
}

module.exports = UserController
