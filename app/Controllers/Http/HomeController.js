'use strict'
const Database = use('Database')

class HomeController {
	async index({ view }) {
		return view.render('home')
	}

	async signup({view}) {
		return view.render('signup')
	}

	async dashboard({ view, auth }) {
		try {
			await auth.getUser()
			const token = await Database.from('tokens').where('user_id', auth.user.id)
			const user = {
				email: auth.user.email,
				token: token[0].token
			}
			return view.render('dashboard', { user })
		} catch(error) {
			return view.render('dashboard', { error })
		}
	}

	async login({ view }) {
		return view.render('login')
	}
}

module.exports = HomeController
