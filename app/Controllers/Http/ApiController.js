'use strict'
const Filter = require('bad-words')
const leoProfanity = require('leo-profanity')
const frenchBadwordsList = require('french-badwords-list')
leoProfanity.clearList()
leoProfanity.add(frenchBadwordsList.array)
const Wordnet = require('node-wordnet')
const wordnet = new Wordnet()
const Sentiment = require('sentiment')
const sentiment = new Sentiment()

const Database = use('Database')

class ApiController {
  async validate({
    request,
    response
  }) {
    const auth = request.header('Authorization')
    if (!auth) {
      return response.json({
        success: false,
        error: 'Wrong token'
      })
    }
    const token = await Database.from('tokens').where('token', auth)
    if (!token[0]) {
      return response.json({
        success: false,
        error: 'Wrong token'
      })
    }
    const payload = request.only(['text', 'features'])
    if (payload.features && payload.text) {
      for await (let feature of payload.features) {
        switch (feature) {
          case 'bad_words':
            if (this._bad_words(payload.text) === true) {
              return response.json({
                success: false,
                error: 'Bad word(s) detected'
              })
            }
          break;

          case 'bad_words_fr':
            if (this._bad_words_fr(payload.text) === true) {
              return response.json({
                success: false,
                error: 'French Bad word(s) detected'
              })
            }
          break;

          case 'english_only':
            this._english_only(payload.text)
            const english = this._english_only
            try {
              await english(payload.text)
            } catch (error) {
              return response.json({
                success: false,
                error: 'Other language than english is detected'
              })
            }
            break;
          
          case 'no_iframe':
            if (this._no_iframe(payload.text) == false) {
              return response.json({
                success: false,
                error: 'An iframe is detected'
              })
            }
            break;

          case 'no_img':
            if (this._no_img(payload.text) == false) {
              return response.json({
                success: false,
                error: 'An image is detected'
              })
            }
            break;

          case 'no_nervouseness':
            if (this._no_nervouseness(payload.text).score < 0) {
              return response.json({
                success: false,
                error: 'The text content is too nervous'
              })
            }
            break;
        }
      }
      return response.json({
        success: true
      })
    } else {
      return response.json({
        success: false,
        error: 'Text or features field is missing'
      })
    }
  }

  _english_only(text) {
    return new Promise((resolve, reject) => {
      const words = text.split(' ')
      if (words.length >= 3) {
        wordnet.lookupAsync(words[2])
          .then(res => {
              if (res[0]) {
                  resolve(true)
              } else {
                reject('No definitions')
              }
          })
          .catch(() => reject('No definitions'))
      }
    })
  }

  _bad_words(text) {
    const filter = new Filter()
    if (filter.isProfane(text)) {
      return true
    } else {
      return false
    }
  }

  _bad_words_fr(text) {
    if (leoProfanity.check(text) === true) {
      return true
    } else {
      return false
    }
  }

  _no_iframe(text) {
    const words = text.split(' ')
    let state = true
    words.forEach(word => {
      if (word == '<iframe') {
        state = false
      }
    })
    return state
  }

  _no_img(text) {
    const words = text.split(' ')
    let state = true
    words.forEach(word => {
      if (word == '<img') {
        state = false
      }
    })
    return state
  }

  _no_nervouseness(text) {
    return sentiment.analyze(text)
  }
}

module.exports = ApiController
