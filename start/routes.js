'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

const Route = use('Route')

Route.get('/', 'HomeController.index')
Route.get('/dashboard', 'HomeController.dashboard')
Route.get('/signup', 'HomeController.signup')
Route.post('/signup', 'UserController.signup')
Route.get('/login', 'HomeController.login')
Route.post('/login', 'UserController.login')
Route.get('/logout', 'UserController.disconnect')
Route.group(() => {
    Route.post('validate', 'ApiController.validate')
}).prefix('api')
