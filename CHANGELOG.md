# Changelog

### 0.0.1

* Initial commit

### 0.0.2

* Add brand

### 0.0.3

* Add License
* Update README.md
* Add .gitignore

### 0.1.3

* Add Signup
* Add login

### 0.2.3

* Update dashboard (add token, personal informations)

### 0.1.4

* Add API Controller

### 0.1.5

* Update Home Page

### 0.2.5

* Add bad_words

### 0.3.5

* Add english_only

### 0.3.6

* Add sitemap
* Update master view

### 0.3.7

* Update master view

### 0.4.7

* Add no_iframe
* Add no_img

### 0.5.7

* Add no_nervouseness
* Add footer
* Add CHANGELOG.md